import java.util.InputMismatchException;
import java.util.Scanner;


class InvalidNumber extends Exception{
    static String invalidNum = "Invalid input. Please input a number greater than 0.";
    public InvalidNumber(){
        super(invalidNum);
    }

}

class GreaterThanMinimum extends Exception{
    static String invalidNum = "Invalid input. Please input a number less than 17.";
    public GreaterThanMinimum(){
        super(invalidNum);
    }

}

public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        try {
            System.out.println("Input an integer whose factorial will be computed: ");
            int num = (in.nextInt());
            if(num <= 0) {
                throw new InvalidNumber();
            } else if (num > 17) {
                throw  new GreaterThanMinimum();
            } else {
                int start = 1;
                int factorial = 1;
                while (num >= start) {
                    factorial *= start;
                    start++;
                }
                System.out.println("The factorial of " + num + " is " + factorial);
            }
        } catch(InputMismatchException e) {
            System.out.println("Only numbers are accepted in this system.");
        } catch (InvalidNumber e){
            System.out.println(e);
        } catch (GreaterThanMinimum e) {
            System.out.println(e);
        }
    }
}
