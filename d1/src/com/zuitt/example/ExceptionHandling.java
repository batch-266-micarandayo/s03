package com.zuitt.example;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ExceptionHandling {
    public static void main(String[] args) {
        /*Exceptions
        * - a problem that arises during the execution of the program
        * - it disrupts the normal flow of the program and terminates abnormally.
        * */

        // Exception Handling
        // refers to managing and catching run-time errors in order to safely run your code.

        Scanner input = new Scanner(System.in);

        int num1 = 0;
        int num2 = 0;



        // try-catch-finally
        try {
            System.out.println("Enter a number1: ");
            num1 = input.nextInt();

            System.out.println("Enter a number2: ");
            num2 = input.nextInt();
            System.out.println("The quotient is: " + num1/num2);
        } catch(ArithmeticException e) {
            System.out.println("Can't divide 0 to a whole number");
        } catch (InputMismatchException e) {
            System.out.println("Invalid input. Please input a number only");
        } finally{
            System.out.println("This will run no matter what!");
        }
    }
}
